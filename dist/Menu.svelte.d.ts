import { SvelteComponentTyped } from "svelte";
import type { ContextMenuMouseEvent } from "./ContextMenuMouseEvent";
import type Settings from "./Settings";
declare const __propDef: {
    props: {
        /** Menu Settings */ settings?: Settings | null | undefined;
        createHandler?: (() => (event: ContextMenuMouseEvent) => void) | undefined;
        show?: ((clickEvent: ContextMenuMouseEvent) => void) | undefined;
    };
    events: {
        [evt: string]: CustomEvent<any>;
    };
    slots: {
        default: {};
    };
};
export type MenuProps = typeof __propDef.props;
export type MenuEvents = typeof __propDef.events;
export type MenuSlots = typeof __propDef.slots;
export default class Menu extends SvelteComponentTyped<MenuProps, MenuEvents, MenuSlots> {
    get createHandler(): () => (event: ContextMenuMouseEvent) => void;
    get show(): (clickEvent: ContextMenuMouseEvent) => void;
}
export {};
